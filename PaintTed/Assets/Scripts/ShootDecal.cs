﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootDecal : MonoBehaviour {

    public GameObject decal;
    public Gradient splatterColorScheme;
    public GameManager score;

    public float timer = 3.0f;

	// Use this for initialization
	void Start ()
    {
        score = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
	}


    void OnCollisionEnter(Collision other)
    {
        Invoke("DestroyBullet", timer);
        if (other.gameObject.tag == "Enviornment")
        {
            score.AddScore();
            ContactPoint contact = other.contacts[0];

            Vector3 newPos = contact.point + (contact.normal.normalized * 0.3f);

            Vector3 decalRotationEuler = Quaternion.LookRotation(contact.normal).eulerAngles;
            decalRotationEuler.z = Random.Range(0, 360);

            Quaternion realRotation = Quaternion.Euler(decalRotationEuler);

            GameObject tempObj = Instantiate(decal, newPos, realRotation);

            tempObj.GetComponent<Renderer>().material.SetColor("_Color", splatterColorScheme.Evaluate(Random.Range(0f, 1f)));

            float value = Random.Range(0.5f, 1.5f);
            tempObj.transform.localScale = new Vector3(value, value, value);
            Destroy(gameObject);
        }

    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Splat")
        {
            score.MinusScore();
        }
    }

    private void DestroyBullet()
    {
        Destroy(gameObject);
    }
}
