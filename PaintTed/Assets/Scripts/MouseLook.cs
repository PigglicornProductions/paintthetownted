﻿using System.Collections;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public float sensitivty = 5.0f;
    public float speed;
    private float realSpeed;

    private float yaw = 0.0f;
    private float pitch = 0.0f;

    // Use this for initialization
    void Start()
    {
        realSpeed = speed * 0.1f;
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.W))
            transform.localPosition += transform.forward * realSpeed;
        if (Input.GetKey(KeyCode.S))
            transform.localPosition += transform.forward * -realSpeed;
        if (Input.GetKey(KeyCode.A))
            transform.localPosition += transform.right * -realSpeed;
        if (Input.GetKey(KeyCode.D))
            transform.localPosition += transform.right * realSpeed;

        yaw += sensitivty * Input.GetAxis("Mouse X");
        pitch -= sensitivty * Input.GetAxis("Mouse Y");

        transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
    }
}

