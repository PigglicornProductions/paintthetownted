﻿using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public GameObject bullet;
    public Camera playerCamera;
    public Gradient bulletColorScheme;
    public float timeBetweenShots;
    public float distance = 10.0f;

    private float timeStamp;

	
	// Update is called once per frame
	void Update ()
    {
		if(Time.time >= timeStamp && Input.GetButton("Fire1"))
        {
            Vector3 gunPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
            gunPosition = Camera.main.ScreenToWorldPoint(gunPosition);
            GameObject tempObj = Instantiate(
                bullet, transform.position, Quaternion.identity);

            tempObj.GetComponent<Rigidbody>().AddForce(
                transform.up.normalized * 20f, ForceMode.Impulse);

            tempObj.GetComponent<Renderer>().material.SetColor(
                "_Color", bulletColorScheme.Evaluate(Random.Range(0f, 1f)));     

            timeStamp = Time.time + timeBetweenShots;
        }
	}
}
