﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public float score = 0;
    public float bouusScore = 0;

	// Use this for initialization
	void Start ()
    {
		
	}

    public void MinusScore()
    {
        if (score > 5)
            score -= 5f;
        else if (score <= 5)
            score = 0;
    }

    public void AddScore()
    {
        score += 10;
    }
}
